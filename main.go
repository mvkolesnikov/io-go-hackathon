package main

import "github.com/gin-gonic/gin"

import (
  "bufio"
  "fmt"
  "log"
  "os"
)

func readLines(path string) ([]string, error) {
  file, err := os.Open(path)
  if err != nil {
    return nil, err
  }
  defer file.Close()

  var lines []string
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    lines = append(lines, scanner.Text())
  }
  return lines, scanner.Err()
}

// writeLines writes the lines to the given file.
func writeLines(lines []string, path string) error {
  file, err := os.Create(path)
  if err != nil {
    return err
  }
  defer file.Close()

  w := bufio.NewWriter(file)
  for _, line := range lines {
    fmt.Fprintln(w, line)
  }
  return w.Flush()
}

/**
 * return 000 to mark the end
 */
func popLineFromFile(path string) (string) {
  lines, err := readLines(path)
  if err != nil {
    log.Fatalf("readLines: %s", err)
  }

  if len(lines) == 0 {
    return "000"
  }

  popedItem := lines[0]
  slicedCollection := lines[1:]
  if err := writeLines(slicedCollection, path); err != nil {
    log.Fatalf("writeLines: %s", err)
  }

  return popedItem
}

// func main() {
//   item := popLineFromFile("storage/session_id_test/movies.txt")
//   fmt.Println(item)
// }

func main() {

	r := gin.Default()

	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	r.GET("/", func(c *gin.Context) {
		c.String(200, "шо хотел?")
	})

	r.Run(":8081")

	//fmt.Println("Go hackathon boilerplate...")
}

